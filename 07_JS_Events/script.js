"use strict";


/* Вывод значения полей по клику на "Войти" на странице авторизации*/

const submit_button = document.querySelector('.send-data');
submit_button.addEventListener('click', function () {
  let login_data = document.getElementById('login').value;
  let pass_data = document.getElementById('pass').value;
  console.log('login: ' + login_data);
  console.log('password: ' + pass_data);
});

/*Проверка ввода в поля Логин и Пароль*/

const login_input = document.querySelector("#login");
const pass_input = document.querySelector("#pass");
const allowed_chars = /[A-Za-z0-9_\.]+/;
pass_input.addEventListener("beforeinput", txt => {
  if (!allowed_chars.test(txt.data)) {
    txt.preventDefault();
  }
});
login_input.addEventListener("beforeinput", txt => {
  if (!allowed_chars.test(txt.data)) {
    txt.preventDefault();
  }
});